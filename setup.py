from distutils.core import setup

setup(
    name='xls2db',
    version='0.4',
    packages=['xls2dblib'],
    scripts=['xls2db.py'],
    url='https://git.math.uzh.ch/utilities/xsl2db',
    license='',
    author='Rafael Ostertag',
    author_email='rafael.ostertag@math.uzh.ch',
    description='Import Excel Workbook into MySQL Database',
    install_requires=[
        'MySQL-python',
        'pyexcel',
        'pyexcel-xls',
        'pyexcel-xlsx'
    ]
)
