import unittest
import os.path

import xls2dblib.excel

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'


class TestExcel(unittest.TestCase):
    def setUp(self):
        self.current_directory = os.path.dirname(__file__)
        self.fixtures_directory = os.path.join(self.current_directory, 'fixtures')
        self.test_workbook_xsl_filename = os.path.join(self.fixtures_directory, 'TestBook.xls')
        self.test_workbook_xslx_filename = os.path.join(self.fixtures_directory, 'TestBook.xlsx')

    def test_nonexistent_file(self):
        with self.assertRaises(IOError):
            excelFile = xls2dblib.excel.ExcelFile(os.path.join(self.fixtures_directory, 'nonexistent_.xls'))

    def test_data_xls_default(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xsl_filename)
        data = excelFile.get_data_from_sheet('Sheet1')

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['a', 'b', 'c'], data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(6, len(data['data']))

    def test_data_xls_skip(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xsl_filename)
        data = excelFile.get_data_from_sheet('Sheet1', 2)

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['4', '5', '6'], data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(4, len(data['data']))

    def test_data_xls_skip_no_header(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xsl_filename)
        data = excelFile.get_data_from_sheet('Sheet1', 2, False)

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNone(data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(5, len(data['data']))

    def test_sparse_data_xsl_default(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xsl_filename)
        data = excelFile.get_data_from_sheet('Sheet2')

        self.assertEqual('Sheet2', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['a', '', '', '', ''], data['header'])
        self.assertEqual(5, data['max_row'])
        self.assertEqual(5, data['min_row'])
        self.assertEqual(1, data['max_data_length'])
        self.assertEqual(5, len(data['data']))

    def test_data_xlsx_default(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xslx_filename)
        data = excelFile.get_data_from_sheet('Sheet1')

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['a', 'b', 'c'], data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        # xslx is different from xsl, so it is the actual length we see in Excel
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(6, len(data['data']))

    def test_data_xlsx_skip(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xslx_filename)
        data = excelFile.get_data_from_sheet('Sheet1', 2)

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['4', '5', '6'], data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        # xslx is different from xsl, so it is the actual length we see in Excel
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(4, len(data['data']))

    def test_data_xlsx_skip_no_header(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xslx_filename)
        data = excelFile.get_data_from_sheet('Sheet1', 2, False)

        self.assertEqual('Sheet1', data['sheet_name'])
        self.assertIsNone(data['header'])
        self.assertEqual(3, data['max_row'])
        self.assertEqual(3, data['min_row'])
        # xslx is different from xsl, so it is the actual length we see in Excel
        self.assertEqual(2, data['max_data_length'])
        self.assertEqual(5, len(data['data']))

    def test_sparse_data_xslx_default(self):
        excelFile = xls2dblib.excel.ExcelFile(self.test_workbook_xslx_filename)
        data = excelFile.get_data_from_sheet('Sheet2')

        self.assertEqual('Sheet2', data['sheet_name'])
        self.assertIsNotNone(data['header'])
        self.assertEqual(['a', '', '', '', ''], data['header'])
        self.assertEqual(5, data['max_row'])
        self.assertEqual(5, data['min_row'])
        # xslx is different from xsl, so it is the actual length we see in Excel
        self.assertEqual(1, data['max_data_length'])
        self.assertEqual(5, len(data['data']))
