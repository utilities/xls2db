import unittest
import xls2dblib.db

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

class TestDb(unittest.TestCase):

    def test_int_to_column_name(self):
        self.assertEqual("A", xls2dblib.db.ImportData.int_to_column_name(0))
        self.assertEqual("AA", xls2dblib.db.ImportData.int_to_column_name(26))
        self.assertEqual("AB", xls2dblib.db.ImportData.int_to_column_name(27))
        self.assertEqual("BA", xls2dblib.db.ImportData.int_to_column_name(52))
        self.assertEqual('CA', xls2dblib.db.ImportData.int_to_column_name(78))
