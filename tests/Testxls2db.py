import os.path
import unittest
import xls2db

class TestXls2db(unittest.TestCase):

    def setUp(self):
        self.current_directory = os.path.dirname(__file__)
        self.fixtures_directory = os.path.join(self.current_directory, 'fixtures')



    def test_read_password_from_file_regression(self):
        password = xls2db.read_password(os.path.join(self.fixtures_directory, 'password_regression'))
        self.assertEqual('secret with space ', password)
