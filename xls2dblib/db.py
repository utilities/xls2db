from __future__ import print_function
import logging
import uuid
import sys

try:
    import MySQLdb
except:
    print("MySQL-python missing. Please install.", file=sys.stderr)
    sys.exit(1)

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(__name__)


def _make_database_name(prefix):
    return prefix + "_" + uuid.uuid1().hex


def _make_database_create(name):
    return "CREATE DATABASE IF NOT EXISTS {};".format(name)

def _make_database_drop(name):
    return "DROP DATABASE IF EXISTS {};".format(name)


def create_database(name, drop=False, **kwargs):
    db = MySQLdb.connect(**kwargs)
    logger.debug('Connected to server %s', kwargs['host'])

    cursor = db.cursor()

    if drop:
        sql = _make_database_drop(name)
        cursor.execute(sql)

    sql = _make_database_create(name)
    cursor.execute(sql)

    logger.debug('Created database %s', name)

    return Database(name, **kwargs)


class Database(object):
    def __init__(self, dbname, **kwargs):
        self.database_name = dbname
        self.database_connection = MySQLdb.connect(db=self.database_name, **kwargs)


class ImportData(object):
    def __init__(self, database):
        self.database = database

    def _create_table(self, data):
        sql = ImportData._make_table_create_from_data(data)
        cursor = self.database.database_connection.cursor()
        cursor.execute(sql)
        logger.debug('Created table \'%s\' in \'%s\'', data['sheet_name'], self.database.database_name)
        cursor.close()

    def import_data(self, data):
        if len(data['data']) == 0:
            logger.info('Sheet \'%s\' is empty. Skipping', data['sheet_name'])
            return

        self._create_table(data)
        if data['min_row'] < 1:
            logger.info('Sheet \'%s\' has a row with zero columns. Skipping', data['sheet_name'])
            return

        cursor = self.database.database_connection.cursor()

        count_data = 0
        for row in data['data']:
            values_place_holder = ("%s," * len(row)).rstrip(',')
            sql = 'INSERT INTO `{}` VALUES({})'.format(data['sheet_name'], values_place_holder)
            cursor.execute(sql, tuple(row))
            count_data += 1

        logger.debug('Inserted %s row(s) from \'%s\'', count_data, data['sheet_name'])
        self.database.database_connection.commit()
        logger.debug('Commited transaction')
        cursor.close()

    @staticmethod
    def _create_default_header(data):
        return [ImportData.int_to_column_name(i) for i in range(0, data['max_row'])]

    @staticmethod
    def _fill_holes_in_header(data):
        header_length = len(data['header'])
        header = list()
        for i in range(header_length):
            if not data['header'][i]:
                header.append(ImportData.int_to_column_name(i))
            else:
                header.append(data['header'][i])

        return header

    @staticmethod
    def _make_table_create_from_data(data):
        if not data['header']:
            header = ImportData._create_default_header(data)
        else:
            header = ImportData._fill_holes_in_header(data)

        rows = ["`{0}` VARCHAR({1})".format(name, data['max_data_length']) for name in header]

        return "CREATE TABLE `{0}` ( {1} );".format(data['sheet_name'], ",".join(rows))

    @staticmethod
    def int_to_column_name(val):
        output = ""
        while True:
            remainder = val % 26
            output = ImportData.base_26_digits[remainder] + output
            val = int(val / 26) - 1

            if val < 0:
                break

        return output

    base_26_digits = [chr(a + 65) for a in range(26)]
