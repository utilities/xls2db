from __future__ import print_function
import logging
import sys

try:
    import pyexcel
except:
    print("pyexcel missing. Please install.", file=sys.stderr)
    sys.exit(1)

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(__name__)


class ExcelFile(object):
    def __init__(self, filename):
        self.filename = filename
        self.book = pyexcel.get_book(file_name=self.filename)

    def get_sheets(self):
        return self.book.sheets.keys()

    def get_data_from_sheet(self, sheet_name, skip_above=0, has_header=True):
        logger.debug("Read data from sheet '%s' in file '%s' skipping %s row(s) and expect %s",
                     sheet_name, self.filename, skip_above, "header" if has_header else "no header")
        sheet = self.book[sheet_name]

        return_value = {
            'header': None,
            'data': [],
            'max_row': 0,
            'min_row': 65000,
            'sheet_name': sheet_name,
            'max_data_length': 0
        }

        header_read = False
        for excel_row in sheet.row:
            if skip_above > 0:
                skip_above -= 1
                continue

            if has_header and not header_read:
                return_value['header'] = map(str,excel_row)
                header_read = True
                continue

            # Get the longest value in the row
            max_cell_width = reduce(lambda x, y: max(x, len(unicode(y))), excel_row, 0)

            return_value['max_data_length'] = max(return_value['max_data_length'], max_cell_width)
            return_value['max_row'] = max(return_value['max_row'], len(excel_row))
            return_value['min_row'] = min(return_value['min_row'], len(excel_row))
            return_value['data'].append(excel_row)

        logger.debug("Sheet %s: max row size: %s; max column size: %s; data rows: %s",
                     return_value['sheet_name'],
                     return_value['max_row'],
                     return_value['max_data_length'],
                     len(return_value['data']))
        return return_value
