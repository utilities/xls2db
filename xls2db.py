#!/usr/bin/env python
from __future__ import print_function
import argparse
import getpass
import logging
import sys

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

from xls2dblib import excel
from xls2dblib import db


def parse_argument():
    parser = argparse.ArgumentParser(description="Read Excel Workbook and import into MySQL Database. The database user has to have CREATE rights for databases. The script will output the unique name of the database created on stdout. For each Sheet imported, a correspondingly named table is created in the database.")
    parser.add_argument("--server", help="Database server name", default=None, nargs='?', required=True)
    parser.add_argument("--user", help="Database user name", default=None, nargs='?')
    parser.add_argument("--password", help="Read password from file. Use '-' to read from stdin", default=None,
                        nargs='?')
    parser.add_argument("--sheet",
                        help="""Sheet to load into database. [SHEET] has the following format: <Name[:SkipColumns[:HasHeader]]>, where `Name' is the name of the sheet, `SkipColumns' is the number of columns to skip from top, `HasHeader' can be either `yes' or `no'. `yes' meaning the first row read is to be treated as column header.""",
                        nargs='*')
    parser.add_argument("--excel-file", help="Excel file to read. Can be a XSLX or XSL file.", required=True)
    parser.add_argument("--debug-output", help="Turn on debug output", action='store_true', default=False)
    parser.add_argument("--drop-database", help="Drop existing database", action='store_true', default=False)
    parser.add_argument("--database", help="Name of the database", default=None, required=True)

    return parser.parse_args()


def read_password(filename):
    if filename == '-':
        return getpass.getpass("Please enter password: ")

    with open(filename) as passwordFile:
        return passwordFile.readline().rstrip("\n")


def string_to_bool(string=""):
    string = string.lower()
    if string in ['yes', 'y', '1', 'true']:
        return True
    elif string in ['no', 'n', '0', 'false']:
        return False

    raise ValueError("Unable to translate '{}' into boolean value".format(string))


def parse_sheet_argument(sheet_argument):
    parameters = sheet_argument.split(':')
    if len(parameters) == 1:
        return {
            'sheet': parameters[0],
            'skip': 0,
            'hasHeader': True
        }
    elif len(parameters) == 2:
        return {
            'sheet': parameters[0],
            'skip': int(parameters[1]),
            'hasHeader': True
        }
    elif len(parameters) == 3:
        return {
            'sheet': parameters[0],
            'skip': int(parameters[1]),
            'hasHeader': string_to_bool(parameters[2])
        }

    return None


def import_sheet(database, excel_file, sheet_info):
    data = excel_file.get_data_from_sheet(sheet_info['sheet'],
                                          sheet_info['skip'],
                                          sheet_info['hasHeader'])

    table = db.ImportData(database)
    table.import_data(data)


def make_sheet_info_from_argument(excel_file, sheet_argument):
    if not sheet_argument:
        sheet_argument = excel_file.get_sheets()

    return [parse_sheet_argument(arg) for arg in sheet_argument if parse_sheet_argument(arg)]


if __name__ == "__main__":
    arguments = parse_argument()
    if arguments.debug_output:
        logging.basicConfig(level=logging.DEBUG)

    server_connection_info = {
        'host': arguments.server,
        'read_default_file': "~/.my.cnf"
    }
    if arguments.password:
        server_connection_info['passwd'] = read_password(arguments.password)
    if arguments.user:
        server_connection_info['user'] = arguments.user


    excel_file = excel.ExcelFile(arguments.excel_file)
    database = db.create_database(arguments.database, arguments.drop_database, **server_connection_info)

    sheet_info_list = make_sheet_info_from_argument(excel_file, arguments.sheet)
    for sheet_info in sheet_info_list:
        import_sheet(database, excel_file, sheet_info)

    print(database.database_name)

