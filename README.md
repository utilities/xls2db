Requirements
-------------

 * MySQL-python
 * pyexcel
 * pyexcel-xls
 * pyexcel-xlsx


Installation
------------------

    [root]
    $ apt install python-pip python-mysqldb
    $ pip install pyexcel
    $ pip install pyexcel-xls
    $ pip install pyexcel-xlsx

    [user]
    $ git clone git@git.math.uzh.ch:utilities/xls2db.git
    $ cd xls2db
    $ sudo python setup.py install

    ~/my.conf:
    [client]
    # Default is Latin1, if you need UTF-8 set this (also in server section)
    default-character-set = utf8